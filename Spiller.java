import java.util.ArrayList;

/**
 * Created by anfauske on 20.07.14.
 */
public class Spiller {

    private String navn;
    private ArrayList<Kort> hånd = new ArrayList<Kort>();

    public Spiller(String navn) {
        this.navn = navn;
    }

    public String getNavn() {
        return navn;
    }

    public ArrayList<Kort> getHånd() {
        return hånd;
    }

}
