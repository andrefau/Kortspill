import java.util.ArrayList;

/**
 * Created by anfauske on 17.07.14.
 */
public class Stokk {

    private ArrayList<Kort> kortstokk = new ArrayList<Kort>();

    public Stokk(){
        // Legger til alle kort
        kortstokk.add(new Kort("Ruter", 1));
        kortstokk.add(new Kort("Ruter", 2));
        kortstokk.add(new Kort("Ruter", 3));
        kortstokk.add(new Kort("Ruter", 4));
        kortstokk.add(new Kort("Ruter", 5));
        kortstokk.add(new Kort("Ruter", 6));
        kortstokk.add(new Kort("Ruter", 7));
        kortstokk.add(new Kort("Ruter", 8));
        kortstokk.add(new Kort("Ruter", 9));
        kortstokk.add(new Kort("Ruter", 10));
        kortstokk.add(new Kort("Ruter", 11));
        kortstokk.add(new Kort("Ruter", 12));
        kortstokk.add(new Kort("Ruter", 13));

        kortstokk.add(new Kort("Spar", 1));
        kortstokk.add(new Kort("Spar", 2));
        kortstokk.add(new Kort("Spar", 3));
        kortstokk.add(new Kort("Spar", 4));
        kortstokk.add(new Kort("Spar", 5));
        kortstokk.add(new Kort("Spar", 6));
        kortstokk.add(new Kort("Spar", 7));
        kortstokk.add(new Kort("Spar", 8));
        kortstokk.add(new Kort("Spar", 9));
        kortstokk.add(new Kort("Spar", 10));
        kortstokk.add(new Kort("Spar", 11));
        kortstokk.add(new Kort("Spar", 12));
        kortstokk.add(new Kort("Spar", 13));

        kortstokk.add(new Kort("Hjerter", 1));
        kortstokk.add(new Kort("Hjerter", 2));
        kortstokk.add(new Kort("Hjerter", 3));
        kortstokk.add(new Kort("Hjerter", 4));
        kortstokk.add(new Kort("Hjerter", 5));
        kortstokk.add(new Kort("Hjerter", 6));
        kortstokk.add(new Kort("Hjerter", 7));
        kortstokk.add(new Kort("Hjerter", 8));
        kortstokk.add(new Kort("Hjerter", 9));
        kortstokk.add(new Kort("Hjerter", 10));
        kortstokk.add(new Kort("Hjerter", 11));
        kortstokk.add(new Kort("Hjerter", 12));
        kortstokk.add(new Kort("Hjerter", 13));

        kortstokk.add(new Kort("Kløver", 1));
        kortstokk.add(new Kort("Kløver", 2));
        kortstokk.add(new Kort("Kløver", 3));
        kortstokk.add(new Kort("Kløver", 4));
        kortstokk.add(new Kort("Kløver", 5));
        kortstokk.add(new Kort("Kløver", 6));
        kortstokk.add(new Kort("Kløver", 7));
        kortstokk.add(new Kort("Kløver", 8));
        kortstokk.add(new Kort("Kløver", 9));
        kortstokk.add(new Kort("Kløver", 10));
        kortstokk.add(new Kort("Kløver", 11));
        kortstokk.add(new Kort("Kløver", 12));
        kortstokk.add(new Kort("Kløver", 13));
    }

    public ArrayList<Kort> getKortstokk() {
        return kortstokk;
    }

}
