import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by anfauske on 17.07.14.
 */
public class Spill {

    private Spiller spiller1;
    private Spiller spiller2;
    private Stokk stokk;
    private Kort øverst;
    private ArrayList<Kort> kortstokk = new ArrayList<Kort>();
    private ArrayList<Kort> brukteKort = new ArrayList<Kort>();

    public Spill(Spiller spiller1, Spiller spiller2, Stokk stokk) {
        this.spiller1 = spiller1;
        this.spiller2 = spiller2;
        this.stokk = stokk;
    }

    public Kort getØverst() {
        return øverst;
    }

    public void setØverst(Kort øverst) {
        this.øverst = øverst;
    }

    public void byttStokk() {
        if (kortstokk.size() == 0) {
            for (Kort k : brukteKort) {
                kortstokk.add(k);
            }
            brukteKort.clear();
        }
    }

    // Hjelpemetode for å dele ut 6 kort til begge spillere
    private void delUt() {
        Random r = new Random();
        kortstokk = stokk.getKortstokk();

        for (int i = 0; i < 6 ; i++) {
            int indeks = r.nextInt(kortstokk.size());
            spiller1.getHånd().add(kortstokk.get(indeks));
            kortstokk.remove(indeks);
        }

        for (int i = 0; i < 6; i++) {
            int indeks = r.nextInt(kortstokk.size());
            spiller2.getHånd().add(kortstokk.get(indeks));
            kortstokk.remove(indeks);
        }

        setØverst(start());
    }

    // Hjelpemetode for å printe ut kort på hånd på en nummerert måte
    private String printHånd(ArrayList<Kort> k) {
        Collections.sort(k, Kort.sortering);
        String tekst = "";
        for (int i = 0; i < k.size(); i++) {
            tekst += i + " - "+ k.get(i) +"\n";
        }
        return tekst;
    }

    // Hjelpemetode for å starte spillet med et kort på bordet
    private Kort start() {
        Random r = new Random();
        int indeks = r.nextInt(kortstokk.size());
        Kort k = kortstokk.get(indeks);
        brukteKort.add(k);
        kortstokk.remove(indeks);
        return k;
    }

    // Sjekker om spilleren ikke velger større eller mindre tall enn antall kort på hånd, og at det er rett sort eller samme tall
    private boolean gyldigSpill(int spiltKort, ArrayList<Kort> hånd, Kort kort){
        if (spiltKort < 0 || spiltKort >= hånd.size()) {
            return false;
        }
        if (hånd.get(spiltKort).getSort().equalsIgnoreCase(kort.getSort()) && hånd.get(spiltKort).getVerdi() != 8) {
            return true;
        }
        if (hånd.get(spiltKort).getVerdi() == kort.getVerdi() && hånd.get(spiltKort).getVerdi() != 8) {
            return true;
        }
        return false;
    }

    // Hjelpemetode for å trekke inn kort
    private void trekkKort(ArrayList<Kort> hånd) {
        Random r = new Random();
        int indeks = r.nextInt(kortstokk.size());
        hånd.add(kortstokk.get(indeks));
        System.out.println("\nDu trakk: " + kortstokk.get(indeks));
        kortstokk.remove(indeks);
    }

    private void turSpiller(Spiller spiller, Kort øverst) {
        Scanner sc = new Scanner(System.in);
        boolean tur = true;
        int trekkTeller = 0;

        while (tur) {
            System.out.println("\nPå bordet:\n" +getØverst()+ "\n\nDin hånd, " + spiller.getNavn() + ":\n" + printHånd(spiller.getHånd()) + "\n\nSkriv 't' for å trekke kort (maks tre) eller 'a' for å " +
                    "avslutte runden. Du har trukket " + trekkTeller + " ganger. Det er " + kortstokk.size() + " kort igjen i kortstokken.\n");

            int spiltKort = -1;
            String kommando = "";

            String input = sc.next();
            try {
                spiltKort = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                kommando = input;
            }

            if (gyldigSpill(spiltKort, spiller.getHånd(), øverst)) {
                setØverst(spiller.getHånd().get(spiltKort));
                brukteKort.add(spiller.getHånd().get(spiltKort));
                spiller.getHånd().remove(spiltKort);
                trekkTeller = 0;
                tur = false;
            } else if (kommando.equalsIgnoreCase("t") && trekkTeller <= 2) {
                if (kortstokk.size() == 0) {
                    byttStokk();
                }
                trekkTeller++;
                trekkKort(spiller.getHånd());
            } else if (kommando.equalsIgnoreCase("a") && trekkTeller > 2) {
                trekkTeller = 0;
                tur = false;
            } else if (spiltKort >= 0 && spiltKort < spiller.getHånd().size()) {
                if (spiller.getHånd().get(spiltKort).getVerdi() == 8) {
                    System.out.println("\nVelg sort (spar, ruter, hjerter eller kløver):\n");

                    brukteKort.add(spiller.getHånd().get(spiltKort));
                    spiller.getHånd().remove(spiltKort);
                    boolean sjekk = true;

                    while (sjekk) {
                        String sort = sc.next();
                        if (sort.equalsIgnoreCase("spar")) {
                            setØverst(new Kort("Spar", 8));
                            trekkTeller = 0;
                            tur = false;
                            sjekk = false;
                        }
                        if (sort.equalsIgnoreCase("ruter")) {
                            setØverst(new Kort("Ruter", 8));
                            trekkTeller = 0;
                            tur = false;
                            sjekk = false;
                        }
                        if (sort.equalsIgnoreCase("hjerter")) {
                            setØverst(new Kort("Hjerter", 8));
                            trekkTeller = 0;
                            tur = false;
                            sjekk = false;
                        }
                        if (sort.equalsIgnoreCase("kløver")) {
                            setØverst(new Kort("Kløver", 8));
                            trekkTeller = 0;
                            tur = false;
                            sjekk = false;
                        }
                        if (!sort.equalsIgnoreCase("spar") && !sort.equalsIgnoreCase("ruter") && !sort.equalsIgnoreCase("hjerter") && !sort.equalsIgnoreCase("kløver")) {
                            System.out.println("\nUgyldig kommando, prøv på nytt.");
                        }
                    }
                } else {
                    System.out.println("\nUgyldig kommando. Prøv på nytt");
                }
            } else {
                System.out.println("\nUgyldig kommando. Prøv på nytt.");
            }
        }
    }

    // Hovedspillet
    public void omgang() {
        delUt();

        boolean ferdig = false;

        while (!ferdig) {

            turSpiller(spiller1, getØverst());

            if (spiller1.getHånd().size() == 0) {
                System.out.println(spiller1.getNavn()+ " har vunnet!");
                break;
            }

            turSpiller(spiller2, getØverst());

            if (spiller2.getHånd().size() == 0) {
                System.out.println(spiller2.getNavn()+ " har vunnet!");
                break;
            }
        }
        System.out.println("\nSpillet er ferdig!");
    }
}