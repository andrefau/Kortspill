import java.util.Comparator;

/**
 * Created by anfauske on 17.07.14.
 */
public class Kort {

    private String sort;
    private int verdi;

    public Kort (String sort, int verdi) {
        this.sort = sort;
        this.verdi = verdi;
    }

    public String getSort() {
        return sort;
    }

    public int getVerdi() {
        return verdi;
    }

    public static Comparator<Kort> sortering = new Comparator<Kort>() {

        @Override
        public int compare(Kort k1, Kort k2) {
            String sort1 = k1.getSort().toUpperCase();
            String sort2 = k2.getSort().toUpperCase();

            if (sort1.equalsIgnoreCase(sort2)) {
                int verdi = k1.getVerdi();
                int verdi2 = k2.getVerdi();
                return verdi - verdi2;
            }

            return sort1.compareTo(sort2);
        }
    };

    public String toString() {
        if (verdi == 1) {
            return sort +" ess";
        }
        if (verdi == 11) {
            return sort +" knekt";
        }
        if (verdi == 12) {
            return sort +" dame";
        }
        if (verdi == 13) {
            return sort +" konge";
        }
        return sort +" "+ verdi;
    }
}
